package com.enablersinc.revolution.helpers;

import android.content.Context;

public class PermissonManager {
     private  Context context;
     public static PermissonManager permissonManager;

    public PermissonManager(Context context) {
        this.context = context;
    }

    public static PermissonManager getInstance(Context context){
        if(permissonManager==null)
            permissonManager=new PermissonManager(context);
        return permissonManager;
    }


}
