package com.enablersinc.revolution.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;

import com.enablersinc.revolution.enumerations.StateOfRouting;
import com.enablersinc.revolution.helpers.DBController;
import com.enablersinc.revolution.helpers.VolleySingleton;
import com.enablersinc.revolution.interfaces.VolleyResponse;
import com.enablersinc.revolution.models.DriveTestPaths;
import com.enablersinc.revolution.models.Path;
import com.enablersinc.revolution.utils.InternetUtils;
import com.google.android.gms.location.LocationListener;


import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.enablersinc.revolution.R;
import com.enablersinc.revolution.helpers.AppConstants;
import com.enablersinc.revolution.models.Pathpoints;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RoutingActivity extends FragmentActivity implements OnMapReadyCallback ,View.OnClickListener{
    private String TAG=RoutingActivity.class.getSimpleName();
    private LocationTracker myLocationTracker=null;
    private StateOfRouting stateOfRouting=null;
    Button btnStartRouting,btnResumeRouting,btnStopRouting;
    ArrayList<Path> executedPath=null;
    DriveTestPaths executedDriveTest;
    private String pathString="";

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        myLocationTracker=new LocationTracker(this);
        executedPath=new ArrayList<>();


        btnStartRouting=(Button)findViewById(R.id.btn_start);
        btnResumeRouting=(Button)findViewById(R.id.btn_resume);
        btnStopRouting=(Button)findViewById(R.id.btn_stop);

        btnStartRouting.setOnClickListener(this);
        btnResumeRouting.setOnClickListener(this);
        btnStopRouting.setOnClickListener(this);

        //Resume and Stop buttons will be disabled untill routing is started
        btnResumeRouting.setEnabled(false);
        btnStopRouting.setEnabled(false);



    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        executedDriveTest =new DriveTestPaths();
     /*   executedDriveTest.setDTID(AppConfig.getInstance(RoutingActivity.this).driveTestPaths.getDTID());
        executedDriveTest.setUserID(AppConfig.getInstance(RoutingActivity.this).driveTestPaths.getUserID());
        executedDriveTest.setDate(AppConfig.getInstance(RoutingActivity.this).driveTestPaths.getDate());
        executedDriveTest.setDTTitle(AppConfig.getInstance(RoutingActivity.this).driveTestPaths.getDTTitle());*/
        executedDriveTest.setDTID(AppConstants.driveTest.getDTID());
        executedDriveTest.setUserID(AppConstants.driveTest.getUserID());
        executedDriveTest.setDate(AppConstants.driveTest.getDate());
        executedDriveTest.setDTTitle(AppConstants.driveTest.getDTTitle());

        if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1){
            checkRequiredPermissions();
        }else mMap.setMyLocationEnabled(true);

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(33.6844, 73.0479);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Islamabad-The Capital"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14f));
        checkRequiredPermissions();
        drawRoute();
    }

    private void drawRoute() {

        for(int i=0;i< AppConstants.driveTest.getPaths().size();i++)
        {
            ArrayList<LatLng> pointsList=new ArrayList<>();
            try {

                PolylineOptions polylineOptions=new PolylineOptions();
                ArrayList<Pathpoints> paths=AppConstants.driveTest.getPaths().get(i).getPathpoints();
                for(int j=0;j<paths.size();j++){
                    Log.e(TAG,paths.get(j).getLat()+","+paths.get(j).getLng()+"");
                    LatLng position=new LatLng(paths.get(j).getLat(),paths.get(j).getLng());
                    pointsList.add(position);
                }
                polylineOptions.addAll(pointsList);
                polylineOptions.color(Color.RED);
                polylineOptions.width(10);
                if(polylineOptions!=null)
                    mMap.addPolyline(polylineOptions);
        LatLng nearestPoint       = findNearestPoint(new LatLng(33.6844, 73.0479),pointsList);

                Log.e("NEAREST POINT: ", "" + nearestPoint); // lat/lng: (3.0,2.0)
                Log.e("DISTANCE: ", "" + SphericalUtil.computeDistanceBetween(new LatLng(33.6844, 73.0479), nearestPoint)); // 222085.35856591124
            } catch (Exception e){
                Log.e(TAG,"Exception in Drawing Path: "+e.toString());
            }


        }
    }

    //TODO , this method finds nearest point directly, not nearest path, check whether to keep this or remove
    private LatLng findNearestPoint(LatLng test, List<LatLng> target) {
        double distance = -1;
        LatLng minimumDistancePoint = test;

        if (test == null || target == null) {
            return minimumDistancePoint;
        }

        for (int i = 0; i < target.size(); i++) {
            LatLng point = target.get(i);

            int segmentPoint = i + 1;
            if (segmentPoint >= target.size()) {
                segmentPoint = 0;
            }

            double currentDistance = PolyUtil.distanceToLine(test, point, target.get(segmentPoint));
            if (distance == -1 || currentDistance < distance) {
                distance = currentDistance;
                minimumDistancePoint = findNearestPoint(test, point, target.get(segmentPoint));
            }
        }

        return minimumDistancePoint;
    }

    /**
     * Based on `distanceToLine` method from
     * https://github.com/googlemaps/android-maps-utils/blob/master/library/src/com/google/maps/android/PolyUtil.java
     */
    private LatLng findNearestPoint(final LatLng p, final LatLng start, final LatLng end) {
        if (start.equals(end)) {
            return start;
        }

        final double s0lat = Math.toRadians(p.latitude);
        final double s0lng = Math.toRadians(p.longitude);
        final double s1lat = Math.toRadians(start.latitude);
        final double s1lng = Math.toRadians(start.longitude);
        final double s2lat = Math.toRadians(end.latitude);
        final double s2lng = Math.toRadians(end.longitude);

        double s2s1lat = s2lat - s1lat;
        double s2s1lng = s2lng - s1lng;
        final double u = ((s0lat - s1lat) * s2s1lat + (s0lng - s1lng) * s2s1lng)
                / (s2s1lat * s2s1lat + s2s1lng * s2s1lng);
        if (u <= 0) {
            return start;
        }
        if (u >= 1) {
            return end;
        }

        return new LatLng(start.latitude + (u * (end.latitude - start.latitude)),
                start.longitude + (u * (end.longitude - start.longitude)));


    }

    private void checkRequiredPermissions() {
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)){
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Required")
                        .setMessage("For better App performance, Location Permission is required.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(RoutingActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.LOCATION_REQUEST_CODE);
                            }
                        }).create().show();

            }else {
                ActivityCompat.requestPermissions(RoutingActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},AppConstants.LOCATION_REQUEST_CODE);

            }
        }
        else mMap.setMyLocationEnabled(true);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case AppConstants.LOCATION_REQUEST_CODE:
            {
                if(grantResults.length>0 ){
                    if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                        mMap.setMyLocationEnabled(true);
                    }
                }
            }
            break;
        }
    }

    private void DrawPath(ArrayList<LatLng> latLngArrayList){

        ArrayList<LatLng> latlng=new ArrayList<LatLng>();
        if(mMap!=null){

            //Add last 2 latlng to draw a ploygon
            latlng.add(latLngArrayList.get(latLngArrayList.size()-2));
            latlng.add(latLngArrayList.get(latLngArrayList.size()-1));

            PolylineOptions polylineOptions = new PolylineOptions().width(10).color(Color.BLUE).geodesic(true);

            //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngArrayList.get(), 14f));
            polylineOptions.addAll(latlng);
            mMap.addPolyline(polylineOptions);


        }



    }

    @Override
    public void onClick(View view) {
        int viewId=view.getId();
        switch (viewId){
            case R.id.btn_start:
            {
                    startRouting();
            }
            break;
            case R.id.btn_resume:{
                pauseOrResumeRouting();
            }
            break;
            case R.id.btn_stop:
            {
                stopRouting();
            }
            break;


        }


    }

    private void stopRouting() {
        btnResumeRouting.setEnabled(false);
        btnStartRouting.setEnabled(false);

        AppConstants.doNotListen=true;

        Path[] paths=new Path[executedPath.size()];
        executedDriveTest.setPaths(executedPath);
        String DTString=new Gson().toJson(executedDriveTest);
        Log.i(TAG,"DTJson :"+DTString);
       // FileLogger.logPublically(TAG,"Routes: "+DriverTestExecute.toString());

        if(InternetUtils.getInstance(RoutingActivity.this).isInternetAvailble()){
            //calling snapToRoad api to adjusting latlong to road
            VolleySingleton.getInstance(RoutingActivity.this).getJsonObject("https://roads.googleapis.com/v1/snapToRoads?path=" + pathString +
                    "&interpolate=true&key=AIzaSyBvd747la4rrx0tKC1qEMQUkOSdQeABoNM", new VolleyResponse() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.e(TAG,"Success: "+response);
                    ArrayList<LatLng> snappedArrayList=new ArrayList<>();
                    JSONObject innerObject;
                    LatLng latLng=null;
                    JSONObject jsonObject=response;
                    JSONArray innerArray= null;
                    try {
                        innerArray = jsonObject.getJSONArray("snappedPoints");
                        for(int i=0;i<innerArray.length();i++){
                            innerObject=innerArray.getJSONObject(i);
                            latLng =new LatLng(innerObject.getJSONObject("location").getDouble("latitude"),innerObject.getJSONObject("location").getDouble("longitude"));
                            snappedArrayList.add(latLng);

                        }

                        if(snappedArrayList.size()>1){
                            PolylineOptions options = new PolylineOptions().width(5).color(Color.GREEN).geodesic(true);
                            for (int z = 0; z < snappedArrayList.size(); z++) {
                                LatLng point = snappedArrayList.get(z);
                                options.add(point);
                            }
                             mMap.addPolyline(options);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                }

                @Override
                public void onFailure(String error) {
                        Log.e(TAG,"error"+error.toString());
                }
            });


            //if internet avialbel, sync data right now
            FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
            final DatabaseReference myRef = firebaseDatabase.getReference("DriveTestExecutes");
            myRef.child("paths").setValue(executedPath);
            Log.e(TAG,"fit");
            try {
                JSONObject dtObject=new JSONObject(DTString);
                saveDTOnline(dtObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else {
            //else save data to local db and sync it later

            Gson gson = new Gson();
            DBController dbController=new DBController(RoutingActivity.this);
            ArrayList<DriveTestPaths> driveTestPaths=new ArrayList<>();
            driveTestPaths.add(executedDriveTest);
            dbController.insertRecord(executedDriveTest.getDTID(),gson.toJson(executedDriveTest));

           // dbController.insertOrUpdateDTJson(driveTestPaths);

/*            dbController.insertRecord(DBController.TABLE_DT_JSON,
                    AppConstants.driveTest.getDTID()
                    ,AppConstants.driveTest.getUserID(),DTString,"executed");*/

        }
    }

    private void saveDTOnline(JSONObject dtObject) {
        VolleySingleton.getInstance(this).postData("http://192.168.1.108:3000/rev_server/insertDT", dtObject, new VolleyResponse() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.e(TAG,"OnlineDtSync:"+"Success");
            }

            @Override
            public void onFailure(String error) {
                Log.e(TAG,"OnlineDtSync:"+"Failure");
            }
        });
    }

    private void pauseOrResumeRouting() {
                if(stateOfRouting==StateOfRouting.START){
                    stateOfRouting=StateOfRouting.PAUSE;
                    btnResumeRouting.setText("Resume");

                }else if(stateOfRouting==StateOfRouting.PAUSE){
                    stateOfRouting=StateOfRouting.RESUME;
                    btnResumeRouting.setText("Pause");
                }else if(stateOfRouting==StateOfRouting.RESUME){
                    stateOfRouting=StateOfRouting.PAUSE;
                    btnResumeRouting.setText("Resume");
                }
    }

    private void startRouting() {
        //TODO : before start routing, make ensure whether location is enabled and are we getting location updates?
        stateOfRouting=StateOfRouting.START;
        btnStartRouting.setEnabled(false);
        btnStopRouting.setEnabled(true);
        btnResumeRouting.setEnabled(true);
        btnResumeRouting.setText("Pause");
    }

    public  class LocationTracker implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status>, LocationListener{

        private final String TAG = this.getClass().getSimpleName();
        private ArrayList<LatLng> latLngArrayList;
        int checkwhenstop=0;
        LocationRequest mLocationRequest;
        GoogleApiClient mGoogleApiClient;
        Context context;
        public LocationTracker(Context context)
        {
            // Build a new GoogleApiClient, specify that we want to use LocationServices
            // by adding the API to the client, specify the connection callbacks are in
            // this class as well as the OnConnectionFailed method.

            this.context = context;
            latLngArrayList=new ArrayList<LatLng>();


            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();

            // This is purely optional and has nothing to do with geofencing.
            // I added this as a way of debugging.
            // Define the LocationRequest.

            mLocationRequest = LocationRequest.create();
            // We want a location update every 1 minute.
            mLocationRequest.setInterval(60000 * 1);
            // We want the location to be as accurate as possible.
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            mGoogleApiClient.connect();
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.i(TAG,"onLocationChanged");
            Log.e(TAG, "Location Information\n"
                    + "==========\n"
                    + "Provider:\t" + location.getProvider() + "\n"
                    + "Lat & Long:\t" + location.getLatitude() + ", "
                    + location.getLongitude() + "\n"
                    + "Altitude:\t" + location.getAltitude() + "\n"
                    + "Bearing:\t" + location.getBearing() + "\n"
                    + "Speed:\t\t" + location.getSpeed() + "\n"
                    + "Accuracy:\t" + location.getAccuracy() + "\n");
            if(AppConstants.doNotListen)
                return;

            if(location != null && (stateOfRouting==StateOfRouting.START || stateOfRouting==StateOfRouting.RESUME))
            {
                checkwhenstop=0;



                AppConstants.CURRENT_LAT = location.getLatitude();
                AppConstants.CURRENT_LONG = location.getLongitude();
                if(latLngArrayList==null)
                    latLngArrayList=new ArrayList<LatLng>();
                if(latLngArrayList.size()<=1){
                    latLngArrayList.add(new LatLng(location.getLatitude(),location.getLongitude()));
                    pathString+=location.getLatitude()+","+location.getLongitude();
                    if(latLngArrayList.size()==1)
                        pathString+="|";
                }

                else {
                    pathString+="|"+location.getLatitude()+","+location.getLongitude();

                    latLngArrayList.add(new LatLng(location.getLatitude(),location.getLongitude()));
                    DrawPath(latLngArrayList);
                    Double angle=getAngle(latLngArrayList.get(latLngArrayList.size()-2).latitude,latLngArrayList.get(latLngArrayList.size()-2).longitude
                            ,location.getLatitude(),location.getLongitude());
                    LatLng current_location = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(current_location).title(angle.toString()));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(current_location));
                    Log.e(TAG,"cal_angl:"+angle);
                    Toast.makeText(getApplicationContext(),"angle: "+angle,Toast.LENGTH_LONG).show();
                }






            }
            if(location !=null && stateOfRouting==StateOfRouting.PAUSE)
            {
             //TODO: constant addition in checkwhenstop can make it figure greater than integer lenght, handle it with a boolena
                checkwhenstop++;
                if(checkwhenstop>0)
                {
                    if(latLngArrayList.size()>1) {
                        Path path = new Path();
                        path.setPathId(1);
                        path.setPathpointswithlist(latLngArrayList);
                        executedPath.add(path);
                        latLngArrayList.clear();
                    }
                }

            }

        }




        @Override
        public void onConnected(@Nullable Bundle bundle) {
            Log.v(TAG, "Connected.");

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED)
            {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }

        }

        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Log.v(TAG, "Connection failed.");
        }

        @Override
        public void onResult(@NonNull Status result) {
            if (result.isSuccess())
            {
                Log.v(TAG, "Success!");
            }
            else if (result.hasResolution())
            {
                Log.v(TAG, "Resolution!");
            }
            else if (result.isCanceled())
            {
                Log.v(TAG, "Canceled");
            }
            else if (result.isInterrupted())
            {
                Log.v(TAG, "Interrupted");
            }

        }
        public double getAngle(double lat1, double lon1, double lat2, double lon2)
        {
            Log.e(TAG,"getAngle-"+lat1+":"+lon1+","+lat2+":"+lon2);
            double dx = lat2 - lat1;
            // Minus to correct for coord re-mapping
            double dy =  Math.cos(Math.PI/180*lat1)*(lon2 - lon1);

            double inRads = Math.atan2(dy,dx);

            if (inRads < 0)
                inRads = Math.abs(inRads);
            else
                inRads = 2*Math.PI - inRads;

            return inRads;
        }
    }

}
