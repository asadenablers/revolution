package com.enablersinc.revolution.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by developer on 1/23/18.
 */

public class Path {

    private int pathId;
    private ArrayList<Pathpoints> pathpoints=new ArrayList<Pathpoints>();


    public int getPathId() {
        return pathId;
    }

    public ArrayList<Pathpoints> getPathpoints() {
        return pathpoints;
    }

    public void setPathId(int pathId) {
        this.pathId = pathId;
    }

    public void setPathpoints(ArrayList<Pathpoints> pathpoints) {
        this.pathpoints = pathpoints;
    }

    public void setPathpointswithlist(ArrayList<LatLng> ArraylistLatLng)
    {

        for(int i=0;i<ArraylistLatLng.size()-1;i++)
        {
            Pathpoints pps=new Pathpoints(ArraylistLatLng.get(i).latitude,ArraylistLatLng.get(i).longitude);
          pathpoints.add(pps);
        }
        this.pathpoints = pathpoints;
    }

    public Path(int pathId, ArrayList<Pathpoints> latLngs) {
        this.pathId = pathId;
        this.pathpoints = latLngs;
    }

    public Path()
    {

    }

}
