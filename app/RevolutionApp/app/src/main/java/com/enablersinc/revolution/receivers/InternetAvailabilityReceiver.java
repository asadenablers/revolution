package com.enablersinc.revolution.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.enablersinc.revolution.app.AppConfig;

public class InternetAvailabilityReceiver extends BroadcastReceiver {
    private  final String TAG = InternetAvailabilityReceiver.class.getSimpleName();
    ConnectivityManager connectivityManager;
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e(TAG,"isInternetAvailable: "+isInternetAvailble(context));
        if(isInternetAvailble(context)){
            //Starting InsertDTs service
            AppConfig.getInstance(context).startJobs(context);
        }

    }
    public  boolean isInternetAvailble(Context context){
        boolean isConnected=false;
        try {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            isConnected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return isConnected;

        }catch (Exception ex){
            Log.e(TAG,"Execption: "+ex);
        }
        return isConnected;
    }
}
