package com.enablersinc.revolution.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class InternetUtils {
    private  final String TAG = InternetUtils.class.getSimpleName();
    Context context;
    public static InternetUtils internetUtils;
    ConnectivityManager connectivityManager;


    public static InternetUtils getInstance(Context context){
        if(internetUtils==null)
            internetUtils=new InternetUtils(context);
        return internetUtils;

    }

    public InternetUtils(Context context) {
        this.context = context;
    }
    //TODO: define this at a single place
    public  boolean isInternetAvailble(){
        boolean isConnected=false;
        try {
            connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            isConnected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return isConnected;

        }catch (Exception ex){
                Log.e(TAG,"Execption: "+ex);
        }
        return isConnected;
    }
}
