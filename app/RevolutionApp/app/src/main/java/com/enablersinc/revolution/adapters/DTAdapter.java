package com.enablersinc.revolution.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enablersinc.revolution.R;
import com.enablersinc.revolution.models.DriveTestPaths;

import java.util.List;

public class DTAdapter  extends RecyclerView.Adapter<DTAdapter.MyViewHolder>{
    private List<DriveTestPaths> driveTestPathsList;
    @NonNull
    @Override
    public DTAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dt_row_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DTAdapter.MyViewHolder holder, int position) {
        DriveTestPaths driveTestPaths=driveTestPathsList.get(position);
        holder.dtTitle.setText(driveTestPaths.getDTTitle());
        holder.dtStatus.setText(driveTestPaths.getDTID()+"");


    }

    @Override
    public int getItemCount() {
        return driveTestPathsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView dtTitle, dtStatus;

        public MyViewHolder(View view) {
            super(view);
            dtTitle = (TextView) view.findViewById(R.id.dtTitle);
            dtStatus = (TextView) view.findViewById(R.id.dtStatus);

        }
    }

    public DTAdapter(List<DriveTestPaths> driveTestPathsList) {
        this.driveTestPathsList = driveTestPathsList;
    }
}
