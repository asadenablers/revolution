package com.enablersinc.revolution.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.enablersinc.revolution.models.DriveTestPaths;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Ikram Hussain on 5/22/18.
 */

public class DBController extends SQLiteOpenHelper {
    private String TAG=DBController.class.getSimpleName();

    Context context;
    //Tables
    public static final String TABLE_DT_JSON = "tblDTJson";
    public static final String TABLE_EXECUTED_DT="tblExecutedDTJson";

    //Colums
    public static final String COL_DT_ID = "_ColDtId";
    public static final String COL_DT_JSON = "ColDTJson";
    public static final String COL_EXECUTED_DT = "ColExectutedDT";
    //SQLite does not have a separate Boolean storage class. Instead, Boolean values are stored as integers 0 (false) and 1 (true).
    public static final String COL_IS_EXECUTED = "ColDTStatus";

    private static final String SQL_CREATE_TBL_DT_JSON =
            "CREATE TABLE " + TABLE_DT_JSON + " (" +
                    COL_DT_ID + " INTEGER PRIMARY KEY," +
                    COL_DT_JSON + " TEXT," +
                    COL_EXECUTED_DT + " TEXT," +
                    COL_IS_EXECUTED + " INTEGER)";
    private static final String SQL_CREATE_TBL_EXECUTED_DT =
            "CREATE TABLE " + TABLE_EXECUTED_DT + " (" +
                    COL_DT_ID + " INTEGER PRIMARY KEY," +
                    COL_EXECUTED_DT + " TEXT )" ;









    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "AirAppDatabase";


    //Constructor





    public DBController(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.context = context;
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
     //on Creation create Tables
        sqLiteDatabase.execSQL(SQL_CREATE_TBL_DT_JSON);
        sqLiteDatabase.execSQL(SQL_CREATE_TBL_EXECUTED_DT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    public void insertRecord(int dtId,String executedDT){
        Log.e(TAG,dtId+"--: "+executedDT );
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_DT_ID,dtId);
        values.put(COL_EXECUTED_DT, executedDT);
        //if unique valule alerady exist, do nothing simply ignore
        long insertRowId=db.insertWithOnConflict(TABLE_EXECUTED_DT,null,values,SQLiteDatabase.CONFLICT_REPLACE);
        Log.i(TAG,"InserteRowId -- :"+insertRowId);

    }
            //this insertOrUpdateDTJson method is called to save or update all DTs, if a DT is already saved in the table, update it
    public void insertOrUpdateDTJson(ArrayList<DriveTestPaths> dtList){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Gson gson = new Gson();
        for (DriveTestPaths driveTestPaths: dtList){
            values.put(COL_DT_ID,driveTestPaths.getDTID());
            values.put(COL_DT_JSON,/*driveTestPaths.getPaths().toString()*/gson.toJson(driveTestPaths).toString());
            values.put(COL_IS_EXECUTED,0);
            //long insertRowId=db.insert(TABLE_DT_JSON,null,values);
            //if unique valule alerady exist, do nothing simply ignore
            long insertRowId=db.insertWithOnConflict(TABLE_DT_JSON,null,values,SQLiteDatabase.CONFLICT_IGNORE);
            Log.e(TAG,"InserteRowId :"+insertRowId);
        }

    }


    public ArrayList<String> retrieveAllDTs(){
        Gson gson=new Gson();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {
                COL_EXECUTED_DT
        };
/*
        String selection=COL_DT_ID+" =?";
        String[] selectionArgs = { userId };*/
        Cursor cursor = db.query(
                TABLE_EXECUTED_DT,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );
        ArrayList<String> resultSet=new ArrayList<>();
        if(cursor.moveToFirst()){
            do{
            Log.e(TAG,"executedJson: "+cursor.getString(0));
             //   resultSet.add(gson.fromJson(cursor.getString(0),DriveTestPaths.class));
                resultSet.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }else {
            //No resuluts found
        }
        Log.i(TAG,"resultSet_Size :"+resultSet.size());
        return resultSet;
    }



    public ArrayList<String> retreiveOneRecord(String tableName, String userId){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {
                COL_DT_JSON
        };

        String selection=COL_DT_ID+" =?";
        String[] selectionArgs = { userId };
        Cursor cursor = db.query(
                TABLE_DT_JSON,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );
        ArrayList<String> resultSet=new ArrayList<>();
        if(cursor.moveToFirst()){
            do{
                resultSet.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }else {
            //No resuluts found
        }
        Log.i(TAG,"resultSet_Size :"+resultSet.size());
        return resultSet;
    }


    public ArrayList<HashMap<String, String>> getData(String tableName) {
        ArrayList<HashMap<String, String>> data = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + tableName;

        SQLiteDatabase database = this.getWritableDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                HashMap<String, String> map = new HashMap<>();

                String[] columnNames = cursor.getColumnNames();

                for (String columnName : columnNames) {
                    map.put(columnName, cursor.getString(cursor.getColumnIndex(columnName)));
                }

                data.add(map);

                cursor.moveToNext();
            }

            cursor.close();
        }

        database.close();

        return data;
    }

    public ArrayList<HashMap<String, String>> getData(String tableName, String where) {
        ArrayList<HashMap<String, String>> data = new ArrayList<>();

        String selectQuery = "Select * from " + tableName + " Where " + where;

        SQLiteDatabase database = this.getWritableDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                HashMap<String, String> map = new HashMap<>();

                String[] columnNames = cursor.getColumnNames();

                for (String columnName : columnNames) {
                    map.put(columnName, cursor.getString(cursor.getColumnIndex(columnName)));
                }

                data.add(map);

                cursor.moveToNext();
            }

            cursor.close();
        }

        database.close();

        return data;
    }

    public String getData(String tableName, String where, String selectedColumn) {
        ArrayList<HashMap<String, String>> data = new ArrayList<>();

        String selectQuery = "Select " + selectedColumn + " from " + tableName + " Where " + where;

        SQLiteDatabase database = this.getWritableDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                HashMap<String, String> map = new HashMap<>();

                String[] columnNames = cursor.getColumnNames();

                for (String columnName : columnNames) {
                    map.put(columnName, cursor.getString(cursor.getColumnIndex(columnName)));
                }

                data.add(map);

                cursor.moveToNext();
            }

            cursor.close();
        }

        database.close();

        return data.size() > 0 ? data.get(0).get(selectedColumn) : "";
    }

    public ArrayList<HashMap<String, String>> searchRecord(String tableName, String keyword, String searchForColumn) {
        ArrayList<HashMap<String, String>> data = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + tableName + " WHERE " + searchForColumn + " like '%" + keyword + "%'";

        SQLiteDatabase database = this.getWritableDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                HashMap<String, String> map = new HashMap<>();

                String[] columnNames = cursor.getColumnNames();

                for (String columnName : columnNames) {
                    map.put(columnName, cursor.getString(cursor.getColumnIndex(columnName)));
                }

                data.add(map);

                cursor.moveToNext();
            }

            cursor.close();
        }

        database.close();

        return data;
    }

    public ArrayList<HashMap<String, String>> searchRecord(String tableName, String where) {
        ArrayList<HashMap<String, String>> data = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + tableName + " WHERE " + where;

        SQLiteDatabase database = this.getWritableDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                HashMap<String, String> map = new HashMap<>();

                String[] columnNames = cursor.getColumnNames();

                for (String columnName : columnNames) {
                    map.put(columnName, cursor.getString(cursor.getColumnIndex(columnName)));
                }

                data.add(map);

                cursor.moveToNext();
            }

            cursor.close();
        }

        database.close();

        return data;
    }

    public void deleteData(String tableName) {
        SQLiteDatabase database = this.getWritableDatabase();

        database.delete(tableName, null, null);

        database.close();
    }

    public int deleteData(String tableName, String where) {
        SQLiteDatabase database = this.getWritableDatabase();

        int rId = database.delete(tableName, where, null);

        database.close();

        return rId;
    }

    /**
     * Get list of Users from SQLite DB as Array List
     *
     * @return cursor
     */
    public Cursor getAllData(String tableName) {
        String selectQuery = "SELECT * FROM " + tableName;

        SQLiteDatabase database = this.getWritableDatabase();

        return database.rawQuery(selectQuery, null);
    }
}
