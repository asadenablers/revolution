package com.enablersinc.revolution.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.enablersinc.revolution.R;
import com.enablersinc.revolution.adapters.DTAdapter;
import com.enablersinc.revolution.helpers.DBController;
import com.enablersinc.revolution.models.DriveTestPaths;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class DriveTestsActivity extends AppCompatActivity {

    private  final String TAG =DriveTestsActivity.class.getSimpleName() ;
    DBController dbController;
    private List<DriveTestPaths> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private DTAdapter dtAdapter;
    ArrayList<DriveTestPaths> driveTestPathsArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_tests);
        driveTestPathsArrayList=new ArrayList<>();
        dbController=new DBController(DriveTestsActivity.this);
        ArrayList<String> resultSet=dbController.retrieveAllDTs();
        Gson gson=new Gson();

        for(String eachResult : resultSet){
            driveTestPathsArrayList.add(gson.fromJson(eachResult,DriveTestPaths.class));
        }
        /*for(String eachValue:resultSet){
            Log.e(TAG,"Resultset:"+eachValue);
        }*/
        recyclerView=(RecyclerView)findViewById(R.id.dtRecyclerView);
        recyclerView.setHasFixedSize(true);

        dtAdapter = new DTAdapter(driveTestPathsArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(dtAdapter);
        dtAdapter.notifyDataSetChanged();




    }
}
