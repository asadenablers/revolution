package com.enablersinc.revolution.models;

/**
 * Created by developer on 1/24/18.
 */

public class Pathpoints
{
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public Pathpoints(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public void setLng(double lng) {
        this.lng = lng;

    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}