package com.enablersinc.revolution.services;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.enablersinc.revolution.helpers.DBController;
import com.enablersinc.revolution.helpers.VolleySingleton;
import com.enablersinc.revolution.interfaces.VolleyResponse;
import com.enablersinc.revolution.models.DriveTestPaths;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class InsertDTs extends JobService {
    public static final String TAG=InsertDTs.class.getSimpleName();
    private DBController dbController;
    @Override
    public boolean onStartJob(JobParameters job) {
        Log.e(TAG,"onStartJob");
        dbController=new DBController(this);
        //TODO: make a service like SyncJobDispatcher.java in CustomerInsight, to sync different data all in one service

        sendExecutedDTs();
        return false;
    }

    private void sendExecutedDTs() {
        //TODO: remove this check
        if(true)
            return;
        Gson gson=new Gson();
        DriveTestPaths driveTestPaths;
        JSONObject eachJsonObject;
        ArrayList<String> driveTestPathsArrayList=dbController.retrieveAllDTs();
        for(String eachResult: driveTestPathsArrayList){
            Log.e(TAG,"DriveTestPath: "+eachResult);
            try {
                 eachJsonObject=new JSONObject(eachResult);
                 VolleySingleton.getInstance(this).postData("http://192.168.1.108:3000/rev_server/insertDT", eachJsonObject, new VolleyResponse() {
                     @Override
                     public void onSuccess(JSONObject response) {
                         Log.e(TAG,"Success "+response.toString());
                     }

                     @Override
                     public void onFailure(String error) {
                            Log.e(TAG,"Failure: "+error.toString());
                     }
                 });
            }catch (Exception e){

            }

        }

    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }
}
