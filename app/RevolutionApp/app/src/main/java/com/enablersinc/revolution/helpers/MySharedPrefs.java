package com.enablersinc.revolution.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPrefs {

    private static MySharedPrefs mySharedPrefs;
    Context context;
    private final String sharedPrefsName="revolution_prefs";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public MySharedPrefs(Context context) {
        this.context = context;
        sharedPreferences=context.getSharedPreferences(sharedPrefsName,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }

    public static MySharedPrefs getInstance(Context context){
        if(mySharedPrefs==null)
            mySharedPrefs=new MySharedPrefs(context);
        return mySharedPrefs;
    }

    public void setStringPrefs(String key,String value){
            editor.putString(key,value);
            editor.commit();
    }
    public String getStringPrefs(String key){
        //if key-value pair is not found, it will return an empty string
        return  sharedPreferences.getString(key,"");
    }
}
