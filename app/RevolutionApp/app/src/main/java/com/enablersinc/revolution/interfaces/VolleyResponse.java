package com.enablersinc.revolution.interfaces;

import org.json.JSONObject;

public interface VolleyResponse {
void onSuccess(JSONObject response);
void onFailure(String error);
}
