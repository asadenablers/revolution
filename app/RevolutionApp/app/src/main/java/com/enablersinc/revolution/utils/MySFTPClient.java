package com.enablersinc.revolution.utils;


import android.content.Context;
import android.util.Log;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

/**
 * Created by umar enablersinc on 13/02/18.
 */

public class MySFTPClient {

    private final String TAG = MySFTPClient.class.getSimpleName();

    private JSch jsch;
    private Session session;
    private ChannelSftp sftpChannel;
    private String mHostIP, mUserName, mPassword;
    private int mPort;

    public MySFTPClient(String mHost, String mUserName, String mPassword, int mPort) {
        this.mHostIP = mHost;
        this.mUserName = mUserName;
        this.mPassword = mPassword;
        this.mPort = mPort;
        this.jsch = null;
        this.session=null;
    }


    public Boolean sftpConnect()
    {
        boolean status = false;
        java.util.Properties config=new java.util.Properties();
        config.put("StrictHostKeyChecking","no");
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(mUserName, mHostIP);
            session.setConfig(config);
            session.setPassword(mPassword);
            session.connect();
            //five seconds if no response from the server
            session.setServerAliveInterval(5*1000);
            sftpChannel = (ChannelSftp) session.openChannel("sftp");
            sftpChannel.connect();
            status=session.isConnected();
        }
        catch (JSchException e) {
            status=false;
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.e(TAG,"status: "+status);
        return status;
    }

    public void sftpDisconnect()
    { if(session.isConnected())
    {
        session.disconnect();
    }
    }

    public boolean ftpUpload(String srcFilePath, String desFileName, String desDirectory, Context context)
    {
        boolean status = false;
        try {
            Log.e(TAG,"source file path: "+srcFilePath+ " destination: "+desDirectory+desFileName);
            sftpChannel.put(srcFilePath,desDirectory+desFileName.trim());
            Log.e(TAG,"File Upload :Success");
            status=true;
        }
        catch (SftpException e) {
            status=false;
            e.printStackTrace();
            Log.e(TAG,"File Upload : Failed");
        }
        return status;
    }
    public long getFileSize(String filePath) throws Exception {
        long fileSize = 0;
        try {
            ChannelSftp sftp = (ChannelSftp) sftpChannel;
            java.util.Vector<ChannelSftp.LsEntry> flLst = sftp.ls(filePath);
            final int i = flLst.size();
            // show the info of every folder/file in the console
            ChannelSftp.LsEntry entry = flLst.get(0);
            SftpATTRS attr = entry.getAttrs();
            return   attr.getSize();
        }

        catch (Exception e)
        {
            e.getStackTrace();
            return  fileSize;
        }
    }

    public Boolean renameFile(String oldFilePath,String newFilePath)
    {
        try {
            ChannelSftp sftp = (ChannelSftp) sftpChannel;
            sftp.rename(oldFilePath,newFilePath);return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
