package com.enablersinc.revolution.models;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by developer on 1/23/18.
 */

public class DriveTestPaths  {

    private int DTID;
    private Date date;
    private String userID;
    private String DTTitle;
    private ArrayList<Path> paths;

    public DriveTestPaths() {
    }

    public DriveTestPaths(int DTID, Date date, String userID, String DTTitle, ArrayList<Path> paths) {
        this.DTID = DTID;
        this.date = date;
        this.userID = userID;
        this.DTTitle = DTTitle;
        this.paths = paths;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setDTTitle(String DTTitle) {
        this.DTTitle = DTTitle;
    }

    public String getUserID() {
        return userID;
    }

    public String getDTTitle() {
        return DTTitle;
    }

    public int getDTID() {
        return DTID;
    }

    public Date getDate() {
        return date;
    }

    public ArrayList<Path> getPaths() {
        return paths;
    }

    public void setDTID(int DTID) {
        this.DTID = DTID;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setPaths(ArrayList<Path> paths) {
        this.paths = paths;
    }

    public String toString() {
        return (this.DTTitle.toString()+"," +this.DTID+" ,"+ this.date+", "+this.paths.size());

    }
}
