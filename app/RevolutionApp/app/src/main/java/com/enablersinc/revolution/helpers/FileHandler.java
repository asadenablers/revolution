package com.enablersinc.revolution.helpers;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collections;
/*
* FileHandler is used to retrieve all file and delete any file
* */

public class FileHandler {
    private String TAG=FileHandler.class.getSimpleName();

    private Context context;
    public  static FileHandler fileHandler;

    public FileHandler(Context context) {
        this.context = context;
    }
        // use this method to get instance of FileHandler,
    public static  FileHandler getInstance(Context context){
        if(fileHandler==null)
            fileHandler=new FileHandler(context);
        return fileHandler;
    }

    public ArrayList<File> retrieveAllFiles(String folderPath){
        ArrayList<File> mFiles = new ArrayList<File>();
        File mDirectory;
        mDirectory = new File(folderPath);
       // ExtensionFilenameFilter filter = new ExtensionFilenameFilter(acceptedFileExtensions);
            Log.e(TAG,"isAvailable :"+mDirectory.exists());
        Log.e(TAG,"isDirectory :"+mDirectory.isDirectory());
        Log.e(TAG,"canRead :"+mDirectory.canRead());

        // Get the files in the directory
        File[] files = mDirectory.listFiles();
        Log.e(TAG,"files"+files.length);
        if (files != null && files.length > 0) {
            for (File f : files) {

                mFiles.add(f);
            }

           // Collections.sort(mFiles, new FileComparator());
        }
        return mFiles;
    }
}
