package com.enablersinc.revolution.helpers;

import android.Manifest;

import com.enablersinc.revolution.models.DriveTestPaths;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by Ikram Hussain on 03/05/2018.
 */

public class AppConstants {
    public static final int LOCATION_REQUEST_CODE=99;
    public static final int PERMISSION_STATE_CODE =88 ;
    public static  double CURRENT_LONG =0.0 ;
    public static  double CURRENT_LAT =0.0 ;
    public static boolean doNotListen=false;
    public static ArrayList<LatLng> snappedLatLng=null;

    public static DriveTestPaths driveTest=null;

    public static String HSOT_IP="192.168.1.71";
    public static String USER_NAME="Administrator";
    public static String PASSWORD="abcd1234_";
    public static int PORT=22;



    //Run-time permissions for MarshMallow +
    public final static String[] PERMISSION_ALL = {
            Manifest.permission.READ_EXTERNAL_STORAGE
         /*   Manifest.permission.CALL_PHONE,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION*/
    };
}
