package com.enablersinc.revolution.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.enablersinc.revolution.R;
import com.enhancell.remote.Connection;
import com.enhancell.remote.Device;
import com.enhancell.remote.GpsLocation;
import com.enhancell.remote.Manager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

// Sample application for Enhancell Remote SDK. The API used here is described in more detail in
// JavaDoc under doc folder
public class EchoMain extends AppCompatActivity implements Manager.Listener, Connection.Listener, Device.Listener, Runnable {
    private static final CharSequence COMMAND_DISCONNECT_DEVICE = "Disconnect";
    private static final CharSequence COMMAND_DIAL = "Dial";
    private static final CharSequence COMMAND_HANGUP = "Hangup";
    private static final CharSequence COMMAND_CONNECT_DATA = "Connect data";
    private static final CharSequence COMMAND_DISCONNECT_DATA = "Disconnect data";
    private static final CharSequence COMMAND_CONNECT_WIFI = "Connect Wi-Fi";
    private static final CharSequence COMMAND_DISCONNECT_WIFI = "Disconnect Wi-Fi";
    private static final CharSequence COMMAND_SEND_SMS = "Send SMS";
    private static final CharSequence COMMAND_TAKE_CONTROL = "Take control";
    private static final CharSequence COMMAND_RELEASE_CONTROL = "Release control";
    private static final CharSequence COMMAND_START_MEASUREMENT = "Start measurement";
    private static final CharSequence COMMAND_STOP_MEASUREMENT = "Stop measurement";
    private static final CharSequence COMMAND_READ_LOCK_STATUS = "Read lock status";

    private class DeviceAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return _devices.size();
        }

        @Override
        public Object getItem(int position) {
            return _devices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            class DeviceView extends ViewGroup {
                private View _layout;
                private Device _device;
                private String _thruput;

                @SuppressLint("ViewHolder")
                public DeviceView(Context context) {
                    super(context);

                    _thruput = "";

                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    _layout = inflater.inflate(R.layout.view_device, null);
                    _layout.setId(R.id.view_device);
                    addView(_layout);
                }

                public void setDevice(Device device) {
                    _device = device;

                    // Step 5. Here we start listening for parameter and state updates etc.
                    setBackgroundColor(adjustSaturation(_device.getColor(), 1.6f));

                    updateValues();
                }

                @Override
                protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
                    _layout.measure(widthMeasureSpec, heightMeasureSpec);

                    setMeasuredDimension(_layout.getMeasuredWidth(), _layout.getMeasuredHeight());
                }

                @Override
                protected void onLayout(boolean changed, int l, int t, int r, int b) {
                    _layout.layout(0, 0, r - l, b - t);
                }

                private void updateValues() {
                    TextView nameText = findViewById(R.id.text_device_name);
                    TextView valueText = findViewById(R.id.text_device_value);
                    TextView detailText = findViewById(R.id.text_device_details);

                    nameText.setText(_device.getTitle());
                    valueText.setText(_thruput);

                    String status = "";
                    status = appendStatus(status, Device.DeviceStates.VoiceCall, "Call");
                    status = appendStatus(status, Device.DeviceStates.Data, "Data");
                    status = appendStatus(status, Device.DeviceStates.WiFi, "Wi-Fi");
                    if (status.isEmpty())
                        status = "Idle";
                    detailText.setText(status);
                }

                private String appendStatus(String str, int state, String text) {
                    if ((_device.getState() & state) == 0)
                        return str;
                    if (!str.isEmpty())
                        str += ", ";
                    return str + text;
                }
            }
            DeviceView view;
            if (convertView != null) {
                view = (DeviceView) convertView;
            } else {
                view = new DeviceView(parent.getContext());
            }
            view.setDevice(_devices.get(position));
            return view;
        }
    }
    private Manager _manager;
    private final List<Connection> _connections = new ArrayList<>();
    private final List<Device> _devices = new ArrayList<>();
    private DeviceAdapter _adapter;
    private Handler _handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.echo_main);

        _handler = new Handler();

        // Step 1. Create instance of the Manager object. Pass in Manager.Listener interface
        // as second parameter to listen for connection updates
        _manager = new Manager(this, this, "RemoteSample");

        _adapter = new DeviceAdapter();
        ListView deviceList = findViewById(R.id.list_devices);
        assert deviceList != null;
        deviceList.setAdapter(_adapter);

        deviceList.setOnItemClickListener((adapter, view, pos, id) -> {
            final Device device = _devices.get(pos);
            int state = device.getState();

            final List<CharSequence> names = new ArrayList<>();
            names.add(COMMAND_TAKE_CONTROL);
            names.add(COMMAND_RELEASE_CONTROL);
            names.add(COMMAND_START_MEASUREMENT);
            names.add(COMMAND_STOP_MEASUREMENT);
            if ((state & Device.DeviceStates.VoiceCall) == 0)
                names.add(COMMAND_DIAL);
            else
                names.add(COMMAND_HANGUP);
            if ((state & Device.DeviceStates.Data) == 0)
                names.add(COMMAND_CONNECT_DATA);
            else
                names.add(COMMAND_DISCONNECT_DATA);
            if ((state & Device.DeviceStates.WiFi) == 0)
                names.add(COMMAND_CONNECT_WIFI);
            else
                names.add(COMMAND_DISCONNECT_WIFI);
            names.add(COMMAND_SEND_SMS);
            names.add(COMMAND_READ_LOCK_STATUS);
            names.add(COMMAND_DISCONNECT_DEVICE);

            new AlertDialog.Builder(EchoMain.this)
                    .setItems(names.toArray(new CharSequence[names.size()]), (dialog, item) -> {
                        try {
                            CharSequence name = names.get(item);
                            if (name.equals(COMMAND_DIAL)) {
                                device.dial(readNumber());
                            } else if (name.equals(COMMAND_HANGUP)) {
                                device.hangup();
                            } else if (name.equals(COMMAND_CONNECT_DATA)) {
                                device.connectData();
                            } else if (name.equals(COMMAND_DISCONNECT_DATA)) {
                                device.disconnectData();
                            } else if (name.equals(COMMAND_CONNECT_WIFI)) {
                                device.connectWiFi();
                            } else if (name.equals(COMMAND_DISCONNECT_WIFI)) {
                                device.disconnectWiFi();
                            } else if (name.equals(COMMAND_SEND_SMS)) {
                                device.sendSms(readNumber(), "Hello!");
                            } else if (name.equals(COMMAND_DISCONNECT_DEVICE)) {
                                device.getConnection().close();
                            } else if (name.equals(COMMAND_TAKE_CONTROL)) {
                                device.setControl(true);
                            } else if (name.equals(COMMAND_RELEASE_CONTROL)) {
                                device.setControl(false);
                            } else if (name.equals(COMMAND_READ_LOCK_STATUS)) {
                                Device.LockStatus bands = device.readLockStatus();
                                if (bands == null) {
                                    Toast.makeText(EchoMain.this, "Failed to read lock status", Toast.LENGTH_SHORT).show();
                                } else {
                                    String text = "";
                                    for (int b : bands.supportedBands) {
                                        if (!text.isEmpty())
                                            text += ",";
                                        text += device.bandToString(b);
                                    }
                                    new AlertDialog.Builder(EchoMain.this)
                                            .setTitle("Bands")
                                            .setMessage(text)
                                            .create().show();
                                }
                            } else if (name.equals(COMMAND_START_MEASUREMENT)) {
                                device.startMeasurement(new SimpleDateFormat("yyyyMMdd-hhmmss", Locale.US).format(new Date()), UUID.randomUUID().toString(), Settings.Secure.getString(getContentResolver(),
                                        Settings.Secure.ANDROID_ID), "projectName", "location", "floor");
                            } else if (name.equals(COMMAND_STOP_MEASUREMENT)) {
                                device.stopMeasurement("");
                            }
                        } catch (Exception e) {
                            Toast.makeText(EchoMain.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }).create().show();
        });
    }

    private String readNumber() throws Exception {
        EditText edit = findViewById(R.id.edit_phone_number);
        if (edit == null)
            return "";
        String number = edit.getText().toString();
        if (number.isEmpty())
            throw new Exception("Empty phone number");
        return number;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.echo_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Step 2. Call Manager's findBluetoothDevices or connectLocal to initiate connect process
        if (item.getItemId() == R.id.action_find_devices) {
            _manager.findBluetoothDevices();
            return true;
        } else if (item.getItemId() == R.id.action_connect_local) {
            _manager.connectLocal();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        for (Connection c : _connections) {
            c.removeListener(this);
            c.close();
        }
        _connections.clear();

        _manager.close();

        super.onDestroy();
    }

    private static int adjustSaturation(int color, float v) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= v;
        return Color.HSVToColor(hsv);
    }

    @Override
    public void newConnection(final Connection connection) {
        // Step 3. When new connection is opened the Listener gets notified, at this point you can
        // start listening for updates on device updates, by calling addListener on the Connection
        // and passing in Connection.Listener interface
        Log.e("MainActivity :","newConnection");
        runOnUiThread(() -> {
            connection.addListener(EchoMain.this);

            _connections.add(connection);

            Toast.makeText(EchoMain.this, "New connection: " + connection.getName(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void deviceConnected(final Device device) {
        device.addListener(this);

        // Step 4. When new devices are detected on the connection this method gets called, at
        // this point we can start listening for updates on device parameters and controlling the
        // devices, see next Step...
        runOnUiThread(() -> {
            _devices.add(device);

            _adapter.notifyDataSetChanged();

            Toast.makeText(EchoMain.this, "Device connected: " + device.getTitle(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void locationUpdate(final Connection connection, final GpsLocation location) {
        // Step 12. This method gets called when connection updates it's location
        Log.e("MainActivity :","locationUpdate");
    }

    @Override
    public void deviceDisconnected(final Device device) {
        device.removeListener(this);

        // Step 13. When device is disconnect this method is called. A good place to do some cleanup
        runOnUiThread(() -> {
            _devices.remove(device);

            _adapter.notifyDataSetChanged();

            Toast.makeText(EchoMain.this, "Device disconnected: " + device.getTitle(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void connectionEnded(final Connection connection) {
        // Step 14. This is the final method that gets called when connection ends.
        runOnUiThread(() -> {
            connection.removeListener(EchoMain.this);

            _connections.remove(connection);

            Toast.makeText(EchoMain.this, "Connection ended: " + connection.getName(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void parameterUpdated(Device device, Device.Value value) {
        // Step 6. When parameter updates this method gets called. Here we check
        // if the parameter in question is DL throughput and if so we print its value.
        // All parameters have unique IDs, refer to parameters.xml in the SDK folder
        // for list of IDs
        Log.e("Device :",device.getModel());
        Log.e("Device_vaue :",value.id+"");
        Log.e("Device_vaue :",value.value.toString()+"");

        Log.e("MainActivity :","parameterUpdated");
        switch (value.id) {
            case 7:
                //_thruput = value.value != null ? String.format(Locale.US, "%d Bps", (int)(Integer) value.value) : "";
                break;
            case 70004:
                if (value.value != null) {
                    android.util.Log.d("RemoteSDK", "LTE channel:" + value.value);
                }
                break;
            case 2:
                if (value.value != null) {
                    android.util.Log.d("RemoteSDK", "Call state:" + value.value);
                }
                break;
        }
        refresh();
    }

    @Override
    public void stateUpdated(Device device, int id, int state) {
        // Step 7. Here we get updates on device state changes, such as call and
        // data states...
        Log.e("MainActivity :","stateUpdated");
        refresh();
    }

    @Override
    public void parameterBlockUpdated(Device device, int id, List<Device.Value> values) {
        // Step 8. Here we get updates on table style parameters, which are a set of
        // parameters that form a logical entity, such as UMTS monitored set...
        Log.e("MainActivity :","parameterBlockUpdated");
    }

    @Override
    public void scriptStatus(Device device, int state, int progress, String text) {
        // Step 9. Here we get updates on script status changes when test scripts
        // are running...
        Log.e("MainActivity :","scriptStatus");
    }

    @Override
    public void eventUpdate(Device device, int id, String description, Map<String, String> args) {
        // Step 10. Here we get updates on events that occur on the device...
        Log.e("MainActivity :","eventUpdate");
    }

    @Override
    public void testResult(Device device, String title, String description, boolean uplink, Map<String,Object> statistics) {
        // Step 11. Here we get updates of test results
        Log.e("MainActivity :","testResult");
    }

    private void refresh() {
        _handler.post(this);
    }

    @Override
    public void run() {
    }
}
