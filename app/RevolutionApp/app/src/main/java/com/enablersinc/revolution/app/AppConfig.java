package com.enablersinc.revolution.app;

import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.multidex.MultiDexApplication;

import com.enablersinc.revolution.models.DriveTestPaths;
import com.enablersinc.revolution.receivers.InternetAvailabilityReceiver;
import com.enablersinc.revolution.services.InsertDTs;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

/**
 * Created by Ikram hussai on 1/22/18.
 */

public class AppConfig extends MultiDexApplication{

    public static final String TAG=AppConfig.class.getSimpleName();

    public AppConfig() {
    }

    private  static AppConfig ourInstance;

    public Context mContext;
    public int scrnWidth, scrnHeight;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    public static DriveTestPaths driveTestPaths;

    @Override
    public void onCreate() {
        super.onCreate();

        //Registering broadcast receiver otherwise it will not be called in Nougat and +
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(new InternetAvailabilityReceiver(),intentFilter);

    //    startJobs(this);
    }

    private AppConfig(Context _mContext) {
        if (_mContext != null) {
            this.mContext = _mContext;
            this.scrnWidth = 0;
            this.scrnHeight = 0;
           // this.driveTestPaths=null;
        }
    }


    public static void initInstance(Context _mContext) {

        if (ourInstance == null) {
            // Create the instance
            ourInstance = new AppConfig(_mContext);
        }
    }

    public static AppConfig getInstance(Context context) {
        if(ourInstance==null)
            initInstance(context);
        return ourInstance;
    }

    public void startJobs(Context context){

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        Job myJob = dispatcher.newJobBuilder()
                .setTag(InsertDTs.TAG)
                .setRecurring(true)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .setService(InsertDTs.class)
                .setTrigger(Trigger.executionWindow(0,60))
                //.setTrigger(Trigger.NOW)
                .setReplaceCurrent(false)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();

        dispatcher.mustSchedule(myJob);
    }
}
