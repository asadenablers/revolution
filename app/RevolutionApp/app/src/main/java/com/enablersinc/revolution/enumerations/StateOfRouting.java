package com.enablersinc.revolution.enumerations;

public enum StateOfRouting {

    START,IDLE,ROUTING,PAUSE,RESUME

}
