package com.enablersinc.revolution.helpers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.enablersinc.revolution.interfaces.VolleyResponse;
import com.google.gson.JsonObject;

import org.json.JSONObject;

public class VolleySingleton {
    private Context context;
    private static VolleySingleton volleySingleton;
    private RequestQueue mRequestQueue;

   private VolleySingleton(Context context){
        this.context=context;
       mRequestQueue = Volley.newRequestQueue(context);
    }

    public static synchronized VolleySingleton getInstance(Context context){
        if(volleySingleton==null)
            volleySingleton=new VolleySingleton(context);
        return volleySingleton;
    }
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

        public void getJsonObject(String url,final VolleyResponse volleyResponse){
            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                        volleyResponse.onSuccess(response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    volleyResponse.onFailure(error.toString());

                }
            });
            getRequestQueue().add(jsonObjectRequest);
        }

        public void postData(String url, JSONObject jsonObject,final VolleyResponse volleyResponse){
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            volleyResponse.onSuccess(response);
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO: Handle error
                            volleyResponse.onFailure(error.toString());

                        }
                    });
        getRequestQueue().add(jsonObjectRequest);
   }

}
