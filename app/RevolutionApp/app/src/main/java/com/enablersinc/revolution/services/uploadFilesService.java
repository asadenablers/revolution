package com.enablersinc.revolution.services;

import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.enablersinc.revolution.app.AppConfig;
import com.enablersinc.revolution.helpers.AppConstants;
import com.enablersinc.revolution.helpers.FileHandler;
import com.enablersinc.revolution.utils.MySFTPClient;

import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
/*
* This service selects files, upload them and deletes uploaded files
* */

public class uploadFilesService extends Service {
    private String TAG=uploadFilesService.class.getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG,"onStartCommand");

        FtpTask ftpTask=new FtpTask();
        ftpTask.execute();
        return START_STICKY;

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    
    private class FtpTask extends AsyncTask<Void,Integer,FTPClient>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e(TAG,"onPreExecute");
        }

        @Override
        protected FTPClient doInBackground(Void... voids) {
            Log.e(TAG,"doInBackground");

            try {
                int retries=5;
                boolean uploadSuccess=true;
                FileHandler fileHandler=FileHandler.getInstance(getApplicationContext());
                String imageTempName=String.valueOf( new Date().getTime())+".txt";
                String serverPath = "/C:/Revolution/";

                MySFTPClient sftpClient = new MySFTPClient(AppConstants.HSOT_IP, AppConstants.USER_NAME, AppConstants.PASSWORD,AppConstants.PORT);
                if(sftpClient.sftpConnect()){
                    if(isCancelled()){

                    }else {
                        boolean result;
                        String folderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/logs";
                        ArrayList<File> fileArrayList=fileHandler.retrieveAllFiles(folderPath);
                        Log.e(TAG,"size :"+fileArrayList.size());
                        for(File  file:fileArrayList){
                            if(file.exists()){
                                Log.e(TAG,file.getPath()+" : "+file.getName());


                                long fileLen = file.length();
                                InputStream in=new FileInputStream(file);
                                result = sftpClient.ftpUpload(file.getPath(), imageTempName.trim(), serverPath, getApplicationContext());
                                Log.e(TAG,"result: "+result);
                                if(result){
                                    long upFileLen = sftpClient.getFileSize(serverPath + imageTempName.trim());
                                    if(fileLen!=upFileLen){
                                        //file uploaded but corrupt
                                        retries--;
                                        if(retries<=0){
                                            uploadSuccess=false;
                                            break;
                                        }


                                    }else {
                                        //file successfully uploaded so rename it on server and delete it from device
                                        sftpClient.renameFile(serverPath+imageTempName,serverPath+file.getName());
                                       // deleteFile(file.getName());
                                        //TODO: we need to add remaining file in another array list and retry to sync them

                                    }
                                }else {
                                    // error in uploading file
                                    Log.e(TAG,"error in uploading files");
                                }


                            }{
                                Log.e(TAG,"file does not exist");
                            }
                        }

                        if(uploadSuccess==false){
                            //Some File were not uploaded
                            Log.e(TAG,"error in uploading files--");
                        }
                    }


                }

                sftpClient.sftpDisconnect();

            }catch (Exception e){
                Log.e(TAG,e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(FTPClient ftpClient) {
            super.onPostExecute(ftpClient);
            Log.e(TAG,"onPostExecute");
        }
    }

}
