package com.enablersinc.revolution.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.enablersinc.revolution.R;
import com.enablersinc.revolution.helpers.AppConstants;
import com.enablersinc.revolution.helpers.DBController;
import com.enablersinc.revolution.helpers.FileHandler;
import com.enablersinc.revolution.helpers.MySharedPrefs;
import com.enablersinc.revolution.helpers.VolleySingleton;
import com.enablersinc.revolution.interfaces.VolleyResponse;
import com.enablersinc.revolution.models.DriveTestPaths;
import com.enablersinc.revolution.services.uploadFilesService;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private String TAG=MainActivity.class.getSimpleName();
    ArrayList<DriveTestPaths> arrayList=null;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    Button btnLogout,btnRoutingActivity,btnSnapToRoad,btnChangeLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.camera);

        drawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);
        navigationView=findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                drawerLayout.closeDrawers();

                int id=item.getItemId();
                switch (id){
                    case R.id.nav_camera:
                    {
                        startActivity(new Intent(MainActivity.this,DriveTestsActivity.class));
                    }
                        break;
                }
                return true;
            }
        });

        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnRoutingActivity=(Button)findViewById(R.id.btnStartMapsActivity);
        btnSnapToRoad=(Button)findViewById(R.id.btnSnapToRoad);
        btnChangeLanguage=(Button)findViewById(R.id.btnChangeLanguage); 
        
        btnLogout.setOnClickListener(this);
        btnRoutingActivity.setOnClickListener(this);
        btnSnapToRoad.setOnClickListener(this);
        btnChangeLanguage.setOnClickListener(this);
        
        arrayList=new ArrayList<DriveTestPaths>();

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("DriveTestPaths");
        //this line will keep data synced, offline storage
        myRef.keepSynced(true);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String uid = user.getUid();
        Query query = myRef.orderByChild("userID").equalTo(uid);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Log.e(TAG,"dataSnapShot: "+dataSnapshot.getValue().toString());
                    for(DataSnapshot userSnapshot : dataSnapshot.getChildren()){

                        Gson gson = new Gson();
                        String s1 = gson.toJson(userSnapshot.getValue());
                        DriveTestPaths dt=gson.fromJson(s1,DriveTestPaths.class);
                        Log.e(TAG,dt.getPaths().size()+"");
                        Log.e(TAG, userSnapshot.child("DTTitle").getValue().toString()+"---");
                        arrayList.add(dt);
                    }
                    Log.e(TAG,arrayList.size()+"----");
                    DBController dbController=new DBController(MainActivity.this);
                    dbController.insertOrUpdateDTJson(arrayList);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG,databaseError.getMessage());

            }
        });
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                Log.e(TAG,"onDrawerSlide");
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                Log.e(TAG,"onDrawerOpened");

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                Log.e(TAG,"onDrawerClosed");

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                Log.e(TAG,"onDrawerStateChanged");

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        getUserPermissions();
    }

    private void getUserPermissions() {
        ArrayList<String> missingPermissions=new ArrayList<>();
        for (int i = 0; i < AppConstants.PERMISSION_ALL.length; i++) {
            if (ContextCompat.checkSelfPermission(this, AppConstants.PERMISSION_ALL[i]) != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(AppConstants.PERMISSION_ALL[i]);
            }
        }

        if(missingPermissions.size()>0){
            ActivityCompat.requestPermissions(this, missingPermissions.toArray(new String[0]), AppConstants.PERMISSION_STATE_CODE);
        }
     }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean greenLight=true;
        switch(requestCode) {
            case AppConstants.PERMISSION_STATE_CODE: {
                if (grantResults.length > 0) {
                     greenLight = true;
                    for(int i=0; i<grantResults.length; i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            greenLight = false;
                        }
                    }
                    if(greenLight){

                    }
                    else{
                        Toast.makeText(this, "Sorry the Application cannot run without you granting us these permissions.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            }
            break;
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int viewId=view.getId();
        switch (viewId){
            case R.id.btnLogout:
                showLogoutDialog();
                break;
            case R.id.btnStartMapsActivity:
            {
                gotoRoutigActivity();
            }
            break;
            case R.id.btnSnapToRoad:
            {
                callSnapToRoadAPI();
            }
            break;
            case R.id.btnChangeLanguage:
            {
                changeLanguage(this);
            }
            break;

        }

    }

    private void changeLanguage(Context context) {


        MySharedPrefs mySharedPrefs=MySharedPrefs.getInstance(this);
        Locale locale;
        if(mySharedPrefs.getStringPrefs("language").equals("en")||mySharedPrefs.getStringPrefs("language").equals("")){
            locale=new Locale("ar");
            mySharedPrefs.setStringPrefs("language","en");
        }else {
            locale=new Locale("en");
            mySharedPrefs.setStringPrefs("language","ar");
        }

        //====================


       // locale = new Locale(language);
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration conf = resources.getConfiguration();
        conf.locale = locale;
        resources.updateConfiguration(conf, dm);



      //  this.setContentView(R.layout.activity_main);



        //====================

     //   String languageToLoad  = "en"; // your language
      // Locale locale = new Locale(languageToLoad);
        /*Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());*/

        /*Locale.setDefault(locale);
        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }*/

    }


    private void callSnapToRoadAPI() {
        //calling snapToRoad api to adjusting latlong to road
        VolleySingleton.getInstance(MainActivity.this).getJsonObject("https://roads.googleapis.com/v1/snapToRoads?path=-35.27801,149.12958|-35.28032,149.12907|-35.28099,149.12929|-35.28144,149.12984|-35.28194,149.13003|-35.28282,149.12956|-35.28302,149.12881|-35.28473,149.12836&interpolate=true&key=AIzaSyBvd747la4rrx0tKC1qEMQUkOSdQeABoNM",
                new VolleyResponse() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.e(TAG,"Success: "+response);

            }

            @Override
            public void onFailure(String error) {
                Log.e(TAG,"error"+error.toString());
            }
        });
    }

    private void gotoRoutigActivity() {
        startService(new Intent(this, uploadFilesService.class));



/*
        AppConstants.driveTest=arrayList.get(0);
        Intent intent=new Intent(MainActivity.this, RoutingActivity.class);
        startActivity(intent);*/
    }

    private void showLogoutDialog(){

        /*AppConfig.getInstance().deleteAllAppData();

        Intent intent = new Intent(this, IntroActivity.class);
        startActivity(intent);
        this.finish();//Not required in the backstack*/

                AlertDialog.Builder b = new AlertDialog.Builder(this);

                b.setTitle("Logout");
                b.setMessage("Are you sure?");

                b.setPositiveButton("YES", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        logout();
                    }
                });

                b.setNegativeButton("NO", null);

                b.show();

        }

    public void logout()
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url

            String name = user.getDisplayName();
            String email = user.getEmail();
            Uri photoUrl = user.getPhotoUrl();

            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getToken() instead.
            String uid = user.getUid();
            FirebaseAuth.getInstance().signOut();
            Intent intent=  new Intent(MainActivity.this,SignInActivitiy.class);

            startActivity(intent);
        }else {
            Intent intent=  new Intent(MainActivity.this,SignInActivitiy.class);

            startActivity(intent);
        }

    }


}
