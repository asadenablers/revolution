package com.enablersinc.revolution.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.enablersinc.revolution.app.AppConfig;

public class eventsReceiver extends BroadcastReceiver {
    private  final String TAG =eventsReceiver.class.getSimpleName() ;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")||
                intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON")){
            Log.e(TAG,"Device PowerdOn");
            //Starting InsertDTs service
            AppConfig.getInstance(context).startJobs(context);

        }
        else if (intent.getAction().equals("android.intent.action.ACTION_SHUTDOWN") ||
                intent.getAction().equals("android.intent.action.QUICKBOOT_POWEROFF")){
            Log.e(TAG,"Device Shutdown");
        }
        else if (intent.getAction().equals("android.intent.action.BATTERY_LOW")){
            Log.e(TAG,"Device Battery Low");
        }
        else if (intent.getAction().equals("android.intent.action.BATTERY_OKAY")) {
            Log.e(TAG,"Device Battery Ok");
        }
        else{
            Log.e(TAG,"Unknow event");
        }
    }
}
